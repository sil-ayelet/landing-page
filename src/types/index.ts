import { NextApiRequest } from "next";
import { DefaultSession, User } from "next-auth";

export interface ExtendedUser extends User {
  role: string;
  permissions: any;
}
export interface ExtendedSession extends Omit<DefaultSession, "user"> {
  user?: ExtendedUser;
}

export interface ExtendedRequest extends NextApiRequest {
  session?: ExtendedSession | null;
}
