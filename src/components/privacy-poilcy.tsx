import { Button, Container } from "@mui/material";
import React from "react";
import { Typography, Link as MuiLink } from "@mui/material";

export const PrivacyPolicy: React.FC = () => {
  return (
    <Container>
      <Typography variant="h4">Privacy Statement</Typography>
      <br />
      <Typography variant="body2" gutterBottom>
        This privacy statement applies to the processing of per­son­al data of
        users of theclo.co website. We are com­mit­ted to pro­tect­ing the
        privacy of anyone who joins our waiting list. <br />
        <br />
        <strong>What per­son­al data we process and why</strong> <br />
        We collect your email and keep it for up to 1 year for the purpose of:{" "}
        <br />
        (1) validating your email <br />
        (2) Send you email notifications and reminders of when our service is up
        and running so you can complete your sign up
        <br />
        (3) To ask for your consent for changes planned in the use of your data.
        <br /> When you sign up for our waiting list, you will get a
        verification email to confirm that the email address provided is correct
        and verifiable. You may with­draw your email from the waiting list by
        emailing us to:{" "}
        <MuiLink
          href="mailto:contact@theclo.co?subject=unsbscribe CLO&body=please remove me from waiting list"
          underline="none"
        >
          contact@theclo.co
        </MuiLink>
        . When you contact us, your email address will be used to complete the
        process of its removal. <br />
        <br />
        <strong>What are we doing about Cook­ies?</strong> <br />
        We might use cookies or similar technologies for analytical purposes.
        <br />
        <br />
        <strong>What about addi­tion­al pur­pos­es?</strong>
        <br /> We will only use your email address data for the pur­pos­es
        described above. However, if we want to use the data for a dif­fer­ent
        pur­pose, we will take appro­pri­ate mea­sures to inform you,
        con­sis­tent with the sig­nif­i­cance of the changes we make and obtain
        your consent if and as required by applic­a­ble data pro­tec­tion law.
        <br />
        <br />
        <strong>You need to be 18 here.</strong>
        <br />
        Our website is nei­ther intend­ed for per­sons aged under 18, and nor do
        we intend to col­lect personal data of website vis­i­tors who are aged
        under 18. However, we are unable to ver­i­fy the age of visitors. If you
        believe we have stored per­son­al data of a minor with­out con­sent,
        please con­tact us via{" "}
        <MuiLink href="mailto:contact@theclo.co" underline="none">
          contact@theclo.co
        </MuiLink>{" "}
        and request to erase such data. In some cir­cum­stances we may anonymize
        your per­son­al data so that it can no longer be asso­ci­at­ed with you,
        in which case we may use such infor­ma­tion with­out fur­ther notice to
        you.
        <br /> If you have any ques­tions on the way we retain your per­son­al
        data you can con­tact us via our{" "}
        <MuiLink href="mailto:contact@theclo.co" underline="none">
          contact@theclo.co
        </MuiLink>{" "}
        <br />
        <br />
        <strong>Shar­ing with oth­ers</strong> <br />
        We try to do everything inhouse but might work with 3rd party
        com­pa­nies to car­ry out the pro­cess­ing described above and to
        com­ply with legal obligations. We will require such third par­ties to
        pro­tect your per­son­al data in accor­dance with the stan­dards set out
        in this pri­va­cy notice, and we take appro­pri­ate mea­sures pur­suant
        to the applic­a­ble data pro­tec­tion law. Such third par­ty
        con­trollers may include law enforce­ment agencies. <br />
        To exer­cise your rights according to this statement, please con­tact us
        via our{" "}
        <MuiLink href="mailto:contact@theclo.co" underline="none">
          contact@theclo.co
        </MuiLink>
        .
        <br />
        <br />
        <strong>Secu­ri­ty</strong>
        <br /> We take appropriate measures to pro­tect your per­son­al data. If
        you feel your per­son­al data is not ade­quate­ly protected or there are
        indications of misuse, please con­tact us by sending an email to:{" "}
        <MuiLink href="mailto:contact@theclo.co" underline="none">
          contact@theclo.co
        </MuiLink>
        . <br />
        <br />
        This privacy statement may be updat­ed and was the last updated on
        12.12.2021.
        <br />
        <br />
      </Typography>
      <br />
    </Container>
  );
};
