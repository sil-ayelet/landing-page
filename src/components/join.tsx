import { getCsrfToken, signIn } from "next-auth/client";
import React, { useCallback, useEffect, useState } from "react";
import {
  LinearProgress,
  Stack,
  TextField,
  Link as MuiLink,
  Typography,
  Box,
} from "@mui/material";
import { Input } from "@mui/material";
import LoadingButton from "@mui/lab/LoadingButton";
import { ComingSoon } from "@src/components/coming-soon";
import { Joined } from "./joined";
import { useRouter } from "next/router";
import { fetcher } from "@src/lib/utils";
import { animated, useSpring } from "react-spring";
import Link from "next/link";
import { useCookies } from "react-cookie";

const SubmitForm: React.FC<{
  handleSubmit: (event: React.FormEvent) => void;
  loading?: boolean;
}> = ({ handleSubmit, loading }) => {
  const [csrfToken, setCsrfToken] = useState<string | null>(null);

  useEffect(() => {
    const getToken = async () => {
      const token = await getCsrfToken();
      setCsrfToken(token);
    };
    getToken();
  }, []);

  const jiggleSpring = useSpring({
    loop: false,
    delay: 3000,
    config: { tension: 200, friction: 5, clamp: true },
    from: { scaleX: 1, scaleY: 1, y: 0 },
    to: [
      { scaleX: 1.1, scaleY: 0.9, y: 3 },
      { scaleX: 1, scaleY: 1, y: 0 },
      { scaleX: 1.1, scaleY: 0.9, y: 3 },
      { scaleX: 1, scaleY: 1, y: 0 },
    ],
  });
  const AnimatedButton = animated(LoadingButton);

  return (
    <>
      {csrfToken ? (
        <form onSubmit={handleSubmit}>
          <Input name="csrfToken" type="hidden" defaultValue={csrfToken} />
          <Stack>
            <TextField
              type="email"
              id="email"
              fullWidth
              required
              label="email"
              name="email"
              autoComplete="off"
              variant="standard"
              color="primary"
            />
            <Typography variant="caption" pb={2} pt={2}>
              By clicking the button below you expressly confirm the acceptance
              of our{" "}
              <Link href="/privacy-statement" passHref>
                <MuiLink underline="none" fontWeight="bold">
                  Privacy Statement
                </MuiLink>
              </Link>
            </Typography>
            <Box textAlign="center">
              <AnimatedButton
                disabled={loading}
                type="submit"
                variant="contained"
                color="secondary"
                loading={loading}
                style={
                  loading
                    ? { minWidth: "50%" }
                    : { minWidth: "50%", ...jiggleSpring }
                }
              >
                Join Waiting List
              </AnimatedButton>
            </Box>
          </Stack>
        </form>
      ) : (
        <Box>
          <LinearProgress />
        </Box>
      )}
    </>
  );
};
export const Join: React.FC = () => {
  const [joined, setJoined] = useState(false);
  const [loading, setLoading] = useState(false);
  const [referCode, setReferCode] = useState("");

  const [email, setEmail] = useState("");
  const { query } = useRouter();
  // const [cookies, setCookie, removeCookie] = useCookies(["hubspotutk"]);
  // const preventHubspotCustomerDedup = useCallback(() => {
  //   removeCookie("hubspotutk", { domain: "hsforms.com" });
  //   removeCookie("hubspotutk", { domain: "hubspot.com" });
  //   removeCookie("hubspotutk", { domain: "theclo.co" });
  //   removeCookie("hubspotutk");
  // }, [removeCookie]);

  const referrer = query.r as string;

  const saveLead = useCallback(async (email: string, referrer?: string) => {
    setLoading(true);

    const res = await fetcher(
      "/api/lead",
      "POST",
      JSON.stringify({
        email,
        referrer,
      })
    );

    setReferCode(res.referrer);
    setLoading(false);
    setJoined(true);

    // fetch(`/api/lead`, {
    //   body: JSON.stringify({
    //     email,
    //     referrer,
    //   }),
    //   headers: {
    //     "Content-Type": "application/json",
    //   },
    //   method: "POST",
    // });
  }, []);

  const handleSubmit = useCallback(
    (event: React.FormEvent) => {
      event.preventDefault();
      setLoading(true);
      // preventHubspotCustomerDedup();
      const target = event.target as typeof event.target & {
        email: { value: string };
      };
      const email = target.email.value;
      setEmail(email);
      saveLead(email, referrer);
      signIn("email", { email, redirect: false });
    },
    [referrer, saveLead]
  );

  return (
    <>
      {joined ? (
        <Joined referCode={referCode} />
      ) : (
        <ComingSoon>
          <SubmitForm handleSubmit={handleSubmit} loading={loading} />
        </ComingSoon>
      )}
    </>
  );
};
