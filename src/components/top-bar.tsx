import * as React from "react";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";

import useScrollTrigger from "@mui/material/useScrollTrigger";
import PopupMenu from "@src/components/popup-menu";
import { Stack, Typography, Tooltip, Button } from "@mui/material";
import { useCurrentUser } from "@src/context/context";
import { Avatar } from "./avatar";
import { signOut } from "next-auth/client";
import { SignIn } from "./sign-in";

export const ElevationScroll: React.FC = ({ children }) => {
  const trigger = useScrollTrigger({
    disableHysteresis: true,
    threshold: 0,
    target: window,
  });

  return React.cloneElement(<>{children}</>, {
    elevation: trigger ? 4 : 0,
  });
};

const UserSection = () => {
  const user = useCurrentUser();

  return (
    <Stack direction="row" spacing={2}>
      {user ? (
        <Tooltip title="Sign Out" placement="bottom">
          <Button onClick={() => signOut()}>
            <Avatar user={user} />
          </Button>
        </Tooltip>
      ) : (
        <SignIn />
      )}
      <Box sx={{ display: { xs: "none", sm: "none", md: "block" } }}>
        <Typography variant="h6">Welcome</Typography>
      </Box>
      <Typography variant="h6">{user?.name || "guest"}</Typography>
    </Stack>
  );
};

export const TopBar: React.FC = ({ children }) => {
  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static" enableColorOnDark>
        <Toolbar>
          <Stack
            direction="row"
            justifyContent="space-between"
            alignItems="center"
            spacing={2}
            width="100%"
          >
            <Stack
              direction="row"
              justifyContent="start"
              alignItems="center"
              spacing={2}
            >
              <UserSection />
              <Tooltip title="Add" placement="top">
                <Button>top</Button>
              </Tooltip>
              {children}
            </Stack>
            <PopupMenu />
          </Stack>
        </Toolbar>
      </AppBar>
    </Box>
  );
};
