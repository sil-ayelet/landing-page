import React from "react";

import { Theme } from "@mui/material";
import { makeStyles } from "@mui/styles";
import Sparkle from "react-sparkle";

const useStyles = makeStyles((theme: Theme) => ({
  overlay: {
    minHeight: "100vh",
    margin: 0,
    padding: 0,
    overflowY: "auto",
    overflowX: "hidden",
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    width: "100%",
    height: "100%",
    display: "block",
    background:
      theme.palette.mode === "dark"
        ? "radial-gradient( circle at top left, black 0, navy 50%, #6C4DEA  100%)"
        : "radial-gradient(circle at top left, white 0, rgba(56, 177, 235,0.5) 50%, rgba(108, 77, 234,0.5) 100%)",

    // zIndex: 0,
  },

  sky: {
    position: "absolute",
    top: 0,
    left: 0,
    height: "100%",
    width: "100%",
    minHeight: "100vh",
    zIndex: -3,
    background:
      theme.palette.mode === "dark"
        ? "radial-gradient( circle at top left, black 0, navy 50%, #6C4DEA  100%)"
        : "radial-gradient(circle at top left, white 0, rgba(56, 177, 235,0.3) 50%, rgba(108, 77, 234,0.3) 100%)",
  },

  clouds: {
    position: "absolute",
    animation: "$move-background 600s linear infinite",
    background: "transparent url(/images/clouds.png) repeat-x top center",
    zIndex: -2,
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    width: "100%",
    height: "100%",
    display: "block",
  },
  stars: {
    animation: "$move-background 3000s linear infinite",

    background: "transparent url(/images/stars.png) repeat top center",
    zIndex: -1,
    position: "absolute",
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    width: "98%",
    height: "95%",
    display: "block",
  },

  "@keyframes twink": {
    "0%": { opacity: 1, animationTimingFunction: "ease-in" },
    "50%": { opacity: 0, animationTimingFunction: "ease-out" },
    "100%": { opacity: 1, animationTimingFunction: "ease-in" },
  },

  "@keyframes move-background": {
    from: { backgroundPosition: "0 0" },
    to: { backgroundPosition: "10000px 0" },
  },
}));

interface SkyBackgroundProps {
  isDarkTheme: boolean;
}
export const SkyBackground: React.FC<SkyBackgroundProps> = ({
  children,
  isDarkTheme,
}) => {
  const classes = useStyles();
  return (
    <>
      <div className={`${classes.overlay} ${classes.sky}`}>
        {isDarkTheme && (
          <>
            <div className={classes.stars} key="stars">
              {" "}
              <Sparkle
                // The color of the sparkles. Can be a color, an array of colors,
                // or 'random' (which will randomly pick from all hex colors).
                color={"#FFF"}
                // The number of sparkles to render. A large number could slow
                // down the page.
                count={150}
                // The minimum and maximum diameter of sparkles, in pixels.
                minSize={1}
                maxSize={3}
                // The number of pixels the sparkles should extend beyond the
                // bounds of the parent element.
                // overflowPx={20}
                // How quickly sparkles disappear; in other words, how quickly
                // new sparkles are created. Should be between 0 and 1000,
                // with 0 never fading sparkles out and 1000 immediately
                // removing sparkles. Most meaningful speeds are between
                // 0 and 150.
                fadeOutSpeed={10}
                // Whether we should create an entirely new sparkle when one
                // fades out. If false, we'll just reset the opacity, keeping
                // all other attributes of the sparkle the same.
                newSparkleOnFadeOut={true}
                // Whether sparkles should have a "flickering" effect.
                flicker={true}
                // How quickly the "flickering" should happen.
                // One of: 'slowest', 'slower', 'slow', 'normal', 'fast', 'faster', 'fastest'
                flickerSpeed={"slowest"}
              />
            </div>
            <div key="sparkle" style={{ maxHeight: "100%" }}></div>
          </>
        )}
        {!isDarkTheme && <div className={classes.clouds} key="clouds"></div>}

        {children}
      </div>
    </>
  );
};
