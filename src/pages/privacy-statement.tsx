import React from "react";
import { PrivacyPolicy } from "@src/components/privacy-poilcy";

const Privacy: React.FC = () => {
  return (
    <>
      <PrivacyPolicy />
    </>
  );
};

export default Privacy;
