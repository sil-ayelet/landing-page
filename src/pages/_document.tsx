import React from "react";
import Document, { Html, Head, Main, NextScript } from "next/document";
import { ServerStyleSheets } from "@mui/styles";
import { lightTheme } from "@src/styles/theme";

export default class MyDocument extends Document {
  private theme = lightTheme;
  render() {
    return (
      <Html lang="en">
        <Head>
          <meta name="theme-color" content={this.theme.palette.primary.main} />
          <meta
            name="description"
            content="A fresh career development tool for women!"
          />
          <meta
            property="og:title"
            content="TheClo.co - A fresh career development tool for women!"
          />
          <meta property="og:type" content="website" />
          <meta property="og:url" content="https://theclo.co" />
          <meta
            property="og:image"
            content="https://theclo.co/images/coming-soon.png"
          />
          <link
            rel="stylesheet"
            href="https://fonts.googleapis.com/css?family=Lato:300,400,500,700&display=swap"
          />
          <link
            rel="stylesheet"
            href="https://fonts.googleapis.com/css?family=Roboto+Slab:300,400,500,700&display=swap"
          />
          <link
            href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,700&display=swap"
            rel="stylesheet"
          />
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}

MyDocument.getInitialProps = async (ctx) => {
  const sheets = new ServerStyleSheets();
  const originalRenderPage = ctx.renderPage;

  ctx.renderPage = () =>
    originalRenderPage({
      enhanceApp: (App) => (props) => sheets.collect(<App {...props} />),
    });

  const initialProps = await Document.getInitialProps(ctx);

  return {
    ...initialProps,
    styles: [
      ...React.Children.toArray(initialProps.styles),
      sheets.getStyleElement(),
    ],
  };
};
