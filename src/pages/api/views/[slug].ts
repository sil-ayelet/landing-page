import type { NextApiRequest, NextApiResponse } from "next";
import connectDB from "@src/middleware/mongodb";
import nc from "next-connect";
import { updateView } from "@src/lib/db-utils";

const handler = nc().post(
  async (req: NextApiRequest, res: NextApiResponse<any>) => {
    try {
      const view = await updateView(req.query.slug as string);

      res.status(200).json({ view });
    } catch (err) {
      res.status(500).send("faild to retrieve users");
    }
  }
);

export default connectDB(handler);
