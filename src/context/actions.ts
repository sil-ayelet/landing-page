import { IUser } from "@src/models/user";
import { Dispatch } from "react";
import { ACCEPT_TERMS, Actions } from "./types";

export const updateUserAcceptTerms =
  (user: IUser) => async (dispatch: Dispatch<Actions>) => {
    const newUser = await userAcceptTermsAction(user);
    dispatch({ type: ACCEPT_TERMS, user: newUser });
  };

const userAcceptTermsAction = async (user: IUser) => {
  const acceptingUser = {
    ...user,
    acceptedTerms: true,
    acceptedTermsAt: Date.now(),
    role: "user",
  };
  try {
    const res = await fetch(`/api/user/${user.email}`, {
      body: JSON.stringify({
        user: acceptingUser,
      }),
      headers: {
        "Content-Type": "application/json",
      },
      method: "POST",
    });

    await res.json();
  } catch {
  } finally {
    // router.push(`/`, undefined, {
    //   shallow: true,
    // });
    return acceptingUser;
  }
};
