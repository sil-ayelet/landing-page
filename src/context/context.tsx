import { IUser } from "@src/models/user";
import { useSession } from "next-auth/client";
import React, {
  createContext,
  useCallback,
  useContext,
  useEffect,
  useReducer,
} from "react";
import useSWR from "swr";
import { userReducer } from "./reducer";
import {
  Actions,
  INIT_STATE,
  UPDATE_CURRENT_USER,
  UPDATE_USERS,
  UsersState,
} from "./types";
import { AccessControl } from "accesscontrol";
import { ExtendedUser } from "@src/types";
import { Session } from "next-auth";

const UserStateContext = React.createContext({} as UsersState);
export const UserDispatchContext = createContext({} as React.Dispatch<Actions>);
export const useDispatch = () => useContext(UserDispatchContext);

const fetcher = (url: string) => fetch(url).then((res) => res.json());
const accessControl = new AccessControl();

export const UserStateProvider: React.FC = (props) => {
  const [session, loading] = useSession();
  const user = session?.user as ExtendedUser;
  const permissions = user?.permissions || {};

  const access = (resource: string, action: string, possession: string) => {
    try {
      const role = user?.role || "guest";

      return accessControl.permission({
        role,
        resource,
        action,
        possession,
      });
    } catch {
      // if role is not a string
      return { granted: false };
    }
  };

  const [state, dispatch] = useReducer(userReducer, {
    loading: true,
    user: undefined,
    access,
  });

  useEffect(() => {
    accessControl.setGrants(permissions);
    return () => {
      accessControl.reset(); // reset when permissions change
    };
  }, [permissions]);

  // useEffect(() => {
  //   dispatch({ type: INIT_STATE, state: value });
  // }, [value]);

  const fetchCurrentUser = async () => {
    if (!session || !session.user) return undefined;
    try {
      const res = await fetch(`/api/user/${session.user.email}`, {
        headers: {
          "Content-Type": "application/json",
        },
        method: "GET",
      });

      const user = (await res.json()).user;
      dispatch({ type: UPDATE_CURRENT_USER, user });
    } catch {
      return undefined;
    }
  };

  // const fetchUsers = async () => {
  //   if (!session || !session.user) return undefined;
  //   try {
  //     const res = await fetch(`/api/user`, {
  //       headers: {
  //         "Content-Type": "application/json",
  //       },
  //       method: "GET",
  //     });

  //     const users = (await res.json()).users;
  //     dispatch({ type: UPDATE_USERS, users });
  //   } catch {
  //     return undefined;
  //   }
  // };

  useEffect(() => {
    fetchCurrentUser();
  }, [session]);

  // useEffect(() => {
  //   fetchUsers();
  // }, [user]);

  return (
    <UserDispatchContext.Provider value={dispatch}>
      <UserStateContext.Provider value={state} {...props} />
    </UserDispatchContext.Provider>
  );
};

export const useUserState = (): UsersState => {
  const context = React.useContext(UserStateContext);
  if (!context) {
    throw new Error("You forgot to wrap GlobalStoreProvider");
  }

  return context;
};

export const useAccess = () => useUserState().access;

export const useCurrentUser = (): IUser | undefined => {
  const { user } = useUserState();
  return user || undefined;
};
