/// <reference types="cypress" />

context("Cookies", () => {
  const token =
    "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkdpbGFkIEJvcm5zdGVpbiIsImlhdCI6MTUxNjIzOTAyMiwidWlkIjoia3VrdSJ9.L4d-ZtMitwJRlWB-Edxzla0m9UgDG925iUzcWE4c9jA";

  beforeEach(() => {
    Cypress.Cookies.debug(true);

    cy.visit(`http://localhost:3000?token=${token}`);
  });

  it("should store token in browser cookie", () => {
    cy.wait(500);
    const cookies = cy.getCookies().then((cookies) => {
      const userCookie = cookies.find((cookie) => cookie.name === "token");
      expect(userCookie).to.have.property("value", token);
    });
  });

  it("should get name from token", () => {
    // The new page should contain an h1 with "About page"
    cy.get("p").contains("Gilad Bornstein");
  });

  describe("invalid token", () => {
    beforeEach(() => {
      const noIdToken =
        "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkdpbGFkIEJvcm5zdGVpbiIsImlhdCI6MTUxNjIzOTAyMn0.Y5gOpzPlisvwU0XDCJrPWGiieD7Qb6RlYwQRT5pOhmE";
      Cypress.Cookies.debug(true);

      cy.visit(`http://localhost:3000?token=${noIdToken}`);
    });

    it("should show 401 error", () => {
      cy.wait(500);
      // The new page should contain 401 error"
      cy.get("p").contains("401");
    });
  });

  it("should store new token in browser cookie", () => {
    cy.visit("http://localhost:3000?token=newToken");
    cy.wait(500);
    const cookies = cy.getCookies().then((cookies) => {
      const userCookie = cookies.find((cookie) => cookie.name === "token");
      expect(userCookie).to.have.property("value", "newToken");
    });
  });
});
